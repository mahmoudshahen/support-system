package android.support.supportsystem.AuthenticationSystem;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.supportsystem.R;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.ProviderQueryResult;

public class SignupActivity extends AppCompatActivity {

    private EditText mInputEmail, mInputPassword ,mInput_Confirm_Password;
    private TextView mSignUp_btn;
    private ProgressBar mProgressBar;
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        //Get Firebase auth instance
        mAuth = FirebaseAuth.getInstance();

        mSignUp_btn = (TextView) findViewById(R.id.sign_up_button);
        mInputEmail = (EditText) findViewById(R.id.email);
        mInputPassword = (EditText) findViewById(R.id.password);
        mInput_Confirm_Password  = (EditText) findViewById(R.id.con_password);
        mProgressBar = (ProgressBar) findViewById(R.id.progressBar);

        mSignUp_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                View view = SignupActivity.this.getCurrentFocus();
                if (view != null) {
                    InputMethodManager inputMethodManager = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
                final String email = mInputEmail.getText().toString().trim();
                final String password = mInputPassword.getText().toString().trim();
                String confirm_password =mInput_Confirm_Password.getText().toString().trim();
                if (TextUtils.isEmpty(email)) {
                    Toast.makeText(getApplicationContext(), "Enter email address!", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (TextUtils.isEmpty(password)) {
                    Toast.makeText(getApplicationContext(), "Enter password!", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (password.length() < 6) {
                    Toast.makeText(getApplicationContext(), "Password too short, enter minimum 6 characters!", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(! password.equals(confirm_password))
                {
                    Toast.makeText(getApplicationContext(), "Confirm Password isn't match !", Toast.LENGTH_SHORT).show();
                    return ;

                }


            else {
                    mProgressBar.setVisibility(View.VISIBLE);
                    //check for availability of the email
                    mAuth.fetchProvidersForEmail(email).addOnCompleteListener(new OnCompleteListener<ProviderQueryResult>() {
                        @Override
                        public void onComplete(@NonNull Task<ProviderQueryResult> task) {
                            Toast.makeText(SignupActivity.this, "fetchProvidersForEmail:onComplete:" + task.isSuccessful(), Toast.LENGTH_SHORT).show();
                            mProgressBar.setVisibility(View.GONE);
                            if(task.isSuccessful()){
                                // getProviders() will return size 1. if email ID is available.
                               if( task.getResult().getProviders().isEmpty())
                               {
                                   Intent intent = new Intent(new Intent(SignupActivity.this, InfoActivity.class));
                                   intent.putExtra("Email",email);
                                   intent.putExtra("Password",password);
                                 Thread thread=new Thread(new Runnable() {
                                       @Override
                                       public void run() {
                                           Log.v("test","first in run");
                                           mAuth.createUserWithEmailAndPassword(email, password)
                                                   .addOnCompleteListener(SignupActivity.this, new OnCompleteListener<AuthResult>() {
                                                       @Override
                                                       public void onComplete(@NonNull Task<AuthResult> task) {
                                                           Toast.makeText(SignupActivity.this, "createUserWithEmail:onComplete:" + task.isSuccessful(), Toast.LENGTH_SHORT).show();
                                                           Log.v("test","in notify");
                                                               if (!task.isSuccessful()) {
                                                                   Toast.makeText(SignupActivity.this, "Authentication failed." + task.getException(),
                                                                           Toast.LENGTH_SHORT).show();
                                                               } else {
                                                                   Toast.makeText(SignupActivity.this, "Done",
                                                                           Toast.LENGTH_SHORT).show();

                                                           }

                                                       }

                                                   });
                                       }
                                   });
                                   thread.start();

                                   startActivity(intent);
                                   finish();
                               }
                                else
                               {
                                   Toast.makeText(SignupActivity.this, "Email is already exist",
                                           Toast.LENGTH_SHORT).show();
                               }
                            }
                            else
                            {
                                Toast.makeText(SignupActivity.this, "connection error or bad email format",
                                        Toast.LENGTH_SHORT).show();
                            }
                        }
                    });

                }

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        mProgressBar.setVisibility(View.GONE);
    }

}