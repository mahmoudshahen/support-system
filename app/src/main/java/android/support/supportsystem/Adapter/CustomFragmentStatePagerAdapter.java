package android.support.supportsystem.Adapter;

import android.os.Bundle;
import android.support.supportsystem.FragmentCharterPage;
import android.support.supportsystem.R;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * Created by Amr on 2/12/2017.
 */

public class CustomFragmentStatePagerAdapter extends FragmentStatePagerAdapter {

    public CustomFragmentStatePagerAdapter(FragmentManager supportFragmentManager) {
        super(supportFragmentManager);
    }

    @Override
    public Fragment getItem(int position) {
        FragmentCharterPage fragment=new FragmentCharterPage();
        Bundle args = new Bundle();

        switch (position) {
            case 0:
                args.putInt("id", R.drawable.charter01);
                break;
            case 1:
                args.putInt("id", R.drawable.charter02);
                break;
            case 2:
                args.putInt("id", R.drawable.charter03);
                break;
            case 3:
                args.putInt("id", R.drawable.charter04);
                break;
            case 4:
                args.putInt("id", R.drawable.charter05);
                break;
            case 5:
                args.putInt("id", R.drawable.charter06);
                break;
            case 6:
                args.putInt("id", R.drawable.charter07);
                break;
            case 7:
                args.putInt("id", R.drawable.charter08);
                break;
            case 8:
                args.putInt("id", R.drawable.charter09);
                break;
            case 9:
                args.putInt("id", R.drawable.charter10);
                break;
            case 10:
                args.putInt("id", R.drawable.charter11);
                break;
            case 11:
                args.putInt("id", R.drawable.charter12);
                break;
            case 12:
                args.putInt("id", R.drawable.charter13);
                break;
            case 13:
                args.putInt("id", R.drawable.charter14);
                break;
            case 14:
                args.putInt("id", R.drawable.charter15);
                break;
            case 15:
                args.putInt("id", R.drawable.charter16);
                break;
            case 16:
                args.putInt("id", R.drawable.charter17);
                break;
            case 17:
                args.putInt("id", R.drawable.charter18);
                break;
            case 18:
                args.putInt("id", R.drawable.charter19);
                break;
            case 19:
                args.putInt("id", R.drawable.charter20);
                break;
            case 20:
                args.putInt("id", R.drawable.charter21);
                break;
            case 21:
                args.putInt("id", R.drawable.charter22);
                break;
            case 22:
                args.putInt("id", R.drawable.charter23);
                break;
            case 23:
                args.putInt("id", R.drawable.charter24);
                break;
            default:
                return null;
        }
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getCount() {
        return 24;
    }
}
