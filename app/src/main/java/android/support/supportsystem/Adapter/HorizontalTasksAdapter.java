package android.support.supportsystem.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.supportsystem.R;
import android.support.supportsystem.activities.TaskDetails;
import android.support.supportsystem.genaric.GenaricData;
import android.support.supportsystem.genaric.anime;
import android.support.supportsystem.model.Task;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.List;

/**
 * Created by gmgn on 2/8/2017.
 */

public class HorizontalTasksAdapter extends RecyclerView.Adapter<HorizontalTasksAdapter.TaskViewHolder>{


    List<Task> taskList;
    FirebaseDatabase firebase;
    Context context;
    int prevoisPosition = 0;
    String memberId;
    public HorizontalTasksAdapter(List<Task> taskList, FirebaseDatabase firebase, Context context)
    {
        this.taskList=taskList;
        this.firebase = firebase;
        this.context = context;
    }
    @Override
    public HorizontalTasksAdapter.TaskViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView= LayoutInflater.from(parent.getContext()).
                inflate(R.layout.taskcard, parent,false);


        return new HorizontalTasksAdapter.TaskViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final HorizontalTasksAdapter.TaskViewHolder holder, final int position) {

        final Task task= taskList.get(position);
        holder.vMonthname.setText(task.getMonthlongname());
        holder.vDayofmonth.setText(task.getDayOfmonth());

     //   holder.title.setText(task.getTitle());
        if(GenaricData.A_S_M == 0)
            holder.close.setVisibility(View.GONE);
        holder.close.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                if(GenaricData.A_S_M != 0) {

                    if(memberId != null)
                    {
                        DatabaseReference databaseReference = firebase.getReference("/Android/myMembers/" + memberId+"/tasksId");
                        databaseReference.child(task.getId()).removeValue();
                        databaseReference = firebase.getReference("/Android/myTasks/"+task.getId()+"/assignedMembers");
                        databaseReference.child(memberId).removeValue();
                        taskList.remove(position);
                        HorizontalTasksAdapter.super.notifyDataSetChanged();
                    }
                    else {
                        DatabaseReference databaseReference = firebase.getReference("/Android/myTasks/" + task.getId() + "/assignedMembers");
                        databaseReference.addChildEventListener(new ChildEventListener() {
                            @Override
                            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                                Log.v("qqqqq", String.valueOf(dataSnapshot.getKey()));
                                DatabaseReference db = firebase.getReference("/Android/myMembers/" + dataSnapshot.getKey() + "/tasksId");
                                db.child(task.getId()).removeValue();

                            }

                            @Override
                            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                            }

                            @Override
                            public void onChildRemoved(DataSnapshot dataSnapshot) {

                            }

                            @Override
                            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });
                        DatabaseReference db = firebase.getReference("/Android/myTasks/" + task.getId());// URL to enter tasks part
                        db.removeValue();
                    }
                }
            }
        });
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, TaskDetails.class);
                intent.putExtra("Task", task);
                context.startActivity(intent);
            }
        });
        if(position>prevoisPosition)
            anime.animate(holder,true);
        else
            anime.animate(holder,false);

        prevoisPosition = position;
    }

    @Override
    public int getItemCount() {
        return taskList.size();
    }

    public static class TaskViewHolder extends RecyclerView.ViewHolder {

        protected TextView vMonthname, vDayofmonth, vDayofweek, title;
        ImageView close;
        CardView cardView;
        public TaskViewHolder(View itemView) {
            super(itemView);
            vMonthname=(TextView) itemView.findViewById(R.id.monthlongdayid);
            vDayofmonth = (TextView)  itemView.findViewById(R.id.dayofmonthid);
            vDayofweek = (TextView) itemView.findViewById(R.id.longdaynameid);
            cardView = (CardView)itemView.findViewById(R.id.card_view_horizontal);
            close = (ImageView) itemView.findViewById(R.id.closeic);
            title=(TextView)itemView.findViewById(R.id.titleid);
        }
    }
    public void setMemberId( String id)
    {
        this.memberId = id;
    }
    public String getMemberId()
    {
        return memberId;
    }
}
