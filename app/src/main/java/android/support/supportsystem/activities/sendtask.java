package android.support.supportsystem.activities;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.supportsystem.R;
import android.support.supportsystem.model.Task;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.FirebaseDatabase;

import java.util.Calendar;
import java.util.Locale;

public class sendtask extends AppCompatActivity {

    private String title;
    private String content;
    private Task task;
    EditText taskContent, taskTitle;
    // EditText  taskDeadLine;
    private FirebaseDatabase firebaseDatabase;
    Button PickMember;
    Button pickDate;
    TextView taskDeadLine;
    String dayLongName;
    String monthlongname;
     int day;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sendtask);
        Toolbar toolbar = (Toolbar) findViewById(R.id.mywelcomebar_three);
        toolbar.setTitle("Send Task");

        taskContent = (EditText) findViewById(R.id.edittextcontentid);
        taskTitle = (EditText) findViewById(R.id.edittexttitleid);
        // taskDeadLine=(EditText)findViewById(R.id.edittextdeadLineid);


        final Calendar calendar = Calendar.getInstance();
         day = calendar.get(Calendar.DAY_OF_MONTH);
         dayLongName = calendar.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.getDefault());
         monthlongname=calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault());
        final int month = calendar.get(Calendar.MONTH);
        final int year = calendar.get(Calendar.YEAR);
        taskDeadLine = (TextView) findViewById(R.id.viewtextdeadLineid);
        pickDate = (Button) findViewById(R.id.pickDateid);
        final DatePickerDialog.OnDateSetListener dateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                taskDeadLine.setText((year + "/" + month + "/" + dayOfMonth));
            }
        };
        pickDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog datePickerDialog = new DatePickerDialog(sendtask.this, dateSetListener, day, month, year);
                datePickerDialog.getDatePicker().setMinDate(calendar.getTimeInMillis());
                datePickerDialog.show();
            }
        });

        PickMember = (Button) findViewById(R.id.PickID);
        firebaseDatabase = FirebaseDatabase.getInstance();

        PickMember.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if ( taskTitle.getText().toString().isEmpty()) {
                    Toast.makeText(getApplicationContext(), "Enter task title!", Toast.LENGTH_SHORT).show();
                    return;
                }
                if ( taskContent.getText().toString().isEmpty()) {
                    Toast.makeText(getApplicationContext(), "Enter task content!", Toast.LENGTH_SHORT).show();
                    return;
                } if ( taskDeadLine.getText().toString().isEmpty()) {
                    Toast.makeText(getApplicationContext(), "pick task deadline !", Toast.LENGTH_SHORT).show();
                    return;
                }
                FragmentManager manager = getSupportFragmentManager();
                SendTaskMethod();
                PickedMemberDialog pickedMemberDialog = new PickedMemberDialog();
                pickedMemberDialog.setTask(task);
                pickedMemberDialog.show(manager, null);


            }
        });
    }

    private void SendTaskMethod() {
        task = new Task();
        task.setContent(taskContent.getText().toString());
        task.setDeadLine(taskDeadLine.getText().toString());
        task.setId("tmp");
        task.setTitle(taskTitle.getText().toString());
        task.setTimeStamp(java.text.DateFormat.getDateTimeInstance()
                .format(Calendar.getInstance().getTime()));
        task.setDayLongName(dayLongName);
        task.setMonthlongname(monthlongname);
        task.setDayOfmonth(String.valueOf(day));
        Toast.makeText(sendtask.this, "from send task method", Toast.LENGTH_SHORT).show();

    }
}