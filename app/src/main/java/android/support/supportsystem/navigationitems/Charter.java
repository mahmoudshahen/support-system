package android.support.supportsystem.navigationitems;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.supportsystem.Adapter.CustomFragmentStatePagerAdapter;
import android.support.supportsystem.R;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by mahmoud shahen on 10/23/2016.
 */

public class Charter extends Fragment {
    ViewPager viewPager;
    CustomFragmentStatePagerAdapter viewPageAdapter;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_charter, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewPager = (ViewPager) view.findViewById(R.id.viewPager);

        viewPageAdapter = new CustomFragmentStatePagerAdapter(getActivity().getSupportFragmentManager());
        viewPager.setAdapter(viewPageAdapter);

    }
}
