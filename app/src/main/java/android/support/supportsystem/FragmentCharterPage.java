package android.support.supportsystem;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

/**
 * Created by Amr on 2/12/2017.
 */

public class FragmentCharterPage extends Fragment {

    ImageView page;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_charter_page, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        if (page == null)//for first time only
        {
            page = (ImageView) view.findViewById(R.id.charterPage);
            Bundle bundle=  this.getArguments();
            page.setImageResource(bundle.getInt("id"));
        }
    }
}

